const fs = require('fs')
const got = require('got');
const log = require('./verboseLog')
const { sendMessage } = require('./settings');
const settings = require('./settings');
const {
    chunk
} = require('./utilities')

async function exportCustomPropertiesToFile() {
    const currentCompanyCustomProperties = await settings.sendMessage(`${settings.baseHubspotUrl}/properties/v1/companies/properties`, {
        // searchParams: {
        //     hapikey: settings.hapikey,
        // },
        headers: {
            'Authorization': `Bearer ${settings.hapikey}`,
            'Content-Type': 'application/json'
          },
        responseType: 'json'
    }).then(response => response.body.filter(properties => properties.createdUserId))

    const currentContactCustomProperties = await settings.sendMessage(`${settings.baseHubspotUrl}/properties/v1/contacts/properties`, {
        // searchParams: {
        //     hapikey: settings.hapikey,
        // },
        headers: {
            'Authorization': `Bearer ${settings.hapikey}`,
            'Content-Type': 'application/json'
          },
        responseType: 'json'
    }).then(response => response.body.filter(properties => properties.createdUserId))

    const properties = {
        companies: currentCompanyCustomProperties,
        contacts: currentContactCustomProperties
    }

    fs.writeFile('./data/customProperties.json',
        JSON.stringify(properties, null, 2), {}, console.log
    )
}

async function importCustomPropertiesFromFile(filePath) {
    const customProperties = require(filePath)
    if (!customProperties) {
        log('No custom properties file to import')
        return
    } //No file just give up.

    const requests = Object.keys(customProperties)
    .map(objectType => {
        log(`Importing ${customProperties[objectType].length} ${objectType} Custom Properties`,1)
        return {
            url: `${settings.baseHubspotUrl}/crm/v3/properties/${objectType}/batch/create`,
            // searchParams:{hapikey: settings.hapikey},
            headers: {
                'Authorization': `Bearer ${settings.hapikey}`,
                'Content-Type': 'application/json'
              },
            json:{
                inputs: customProperties[objectType]
            },
            method: 'post',
            responseType: 'json'
        }
    })
    .map( request => {
        const {url, ...options} = request
        return sendMessage(url,options).then(response=>log(response.body,2)).catch(err => log(err,0))
    })
    Promise.allSettled(requests)
        .then(console.log('Property Types imported'))
        
}

module.exports = {
    exportCustomPropertiesToFile,
    importCustomPropertiesFromFile
}