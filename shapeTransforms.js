const {compact} = require('lodash')

// takes an object matching the original names and returns an object for hubspot
// to create/update a company with

const companyProperties = [
    'hubspotCompanyId',
    'agagt_',
    'approved',
    'name',
    'address',
    'address2',
    'city',
    'state',
    'zip',
    'phone',
    'fax',
    'website',
    'billing_address',
    'billing_address_2',
    'billing_city',
    'billing_state',
    'billing_zip_code',
    'br_type',
    'inactive',
    'region',
    'wp_year0',
    'wp_year1',
    'wp_year2',
    'wp_year3',
    'wp_cnt0',
    'wp_cnt1',
    'wp_cnt2',
    'wp_cnt3',
    'sp_cnt0',
    'sp_cnt1',
    'sp_cnt2',
    'sp_cnt3',
    'group',
    'toBeRemoved',
    'br_code',
    'group_agency__company_',
    'hs_parent_company_id',
    'parent_company_name',
    'br_type1',
]

const contactProperties = [
    'hubspotContactId',
    'agagt_', //Custom
    'firstname',
    'lastname',
    'address',
    'email',
    'city',
    'state',
    'zip',
    'phone',
    'mobilephone',
    'fax', //custom
    'website',
    'email_type', //custom
    'toBeRemoved',
    'br_code',
    'group_agency__company_',
    'hs_parent_company_id',
    'parent_company_name'
]

function transformAIAToHubspotCompanySchema(rawData){

    const schema = {
        companyId: rawData.hubspot_id,
        agagt_: rawData.agagt_, //Custom String
        approved: rawData.approved.toLowerCase(), //Custom Boolean
        name: rawData.company,
        address: rawData.st_addr1,
        address2: rawData.st_addr2,
        city: rawData.st_city,
        state: rawData.st_state,
        zip: rawData.st_zip,
        phone: rawData.phone,
        fax: rawData.fax, //Custom String
        website: rawData.url,
        billing_address: rawData.address1, //Custom String
        billing_address_2: rawData.address2, //Custom String
        billing_city: rawData.city, //Custom String
        billing_state: rawData.state, //Custom String
        billing_zip_code: rawData.zip_code, //Custom String
        br_type1: rawData.br_type, //Custom String
        inactive: rawData.inactive.toLowerCase(), //Custom Checkbox
        region: rawData.region, //Custom String
        wp_year0: rawData.wp_year0, //Custom String
        wp_year1: rawData.wp_year1, //Custom String
        wp_year2: rawData.wp_year2, //Custom String
        wp_year3: rawData.wp_year3, //Custom String
        wp_cnt0: rawData.wp_cnt0, //Custom String
        wp_cnt1: rawData.wp_cnt1, //Custom String
        wp_cnt2: rawData.wp_cnt2, //Custom String
        wp_cnt3: rawData.wp_cnt3, //Custom String
        sp_cnt0: rawData.sp_cnt0, //Custom String
        sp_cnt1: rawData.sp_cnt1, //Custom String
        sp_cnt2: rawData.sp_cnt2, //Custom String
        sp_cnt3: rawData.sp_cnt3, //Custom String
        group: rawData.group, //Custom String
        toBeRemoved: rawData.toBeRemoved, //Custom String
        br_code: rawData.br_code,
        group_agency__company_: rawData.group_agency__company_,
        
    }
    if(!schema.companyId){
        delete schema.companyId
    }
    return schema
}

function transformHubspotToAIACompanySchema(rawData){
    return {
        
        hubspot_id: rawData.companyId,
        agagt_: rawData.agagt_, //Custom String
        approved: rawData.approved, //Custom Boolean
        company: rawData.name,
        st_addr1: rawData.address,
        st_addr2: rawData.address2,
        st_city: rawData.city,
        st_state: rawData.state,
        st_zip: rawData.zip,
        phone: rawData.phone,
        fax: rawData.fax, //Custom String
        url: rawData.website,
        address1: rawData.billing_address, //Custom String
        address2: rawData.billing_address_2, //Custom String
        city: rawData.billing_city, //Custom String
        state: rawData.billing_state, //Custom String
        zip_code: rawData.billing_zip_code, //Custom String
        br_type: rawData.br_type1, //Custom String
        inactive: rawData.inactive, //Custom Checkbox
        region: rawData.region, //Custom String
        wp_year0: rawData.wp_year0, //Custom String
        wp_year1: rawData.wp_year1, //Custom String
        wp_year2: rawData.wp_year2, //Custom String
        wp_year3: rawData.wp_year3, //Custom String
        wp_cnt0: rawData.wp_cnt0, //Custom String
        wp_cnt1: rawData.wp_cnt1, //Custom String
        wp_cnt2: rawData.wp_cnt2, //Custom String
        wp_cnt3: rawData.wp_cnt3, //Custom String
        sp_cnt0: rawData.sp_cnt0, //Custom String
        sp_cnt1: rawData.sp_cnt1, //Custom String
        sp_cnt2: rawData.sp_cnt2, //Custom String
        sp_cnt3: rawData.sp_cnt3, //Custom String
        group: rawData.group, //Custom String
        toBeRemoved: rawData.toBeRemoved, //Custom String
        br_code: rawData.br_code,
        group_agency__company_:rawData.group_agency__company_,
        hs_parent_company_id: rawData.hs_parent_company_id,
        parent_company_name: rawData.parent_company_name,
        // br_type1: rawData.br_type, //Custom String
    }
}

function transformAIAToHubspotContactSchema(rawData){
    return {
        id: rawData.hubspot_id,
        agagt_: rawData.agagt_, //Custom
        company: rawData.company,
        firstname: rawData.contact2,
        lastname: rawData.contact3,
        email: rawData.email_addr,
        address: rawData.address,
        city: rawData.city,
        state: rawData.state,
        zip: rawData.zip_code,
        phone: rawData.phone,
        mobilephone: rawData.cell_phone,
        fax: rawData.fax, //Custom
        website: rawData.url,
        email_type: rawData.email_type, //Custom
        toBeRemoved:rawData.toBeRemoved,
        br_code:rawData.br_code,
        group_agency__company_: rawData.group_agency__company_,
        hs_parent_company_id: rawData.hs_parent_company_id,
        parent_company_name: rawData.parent_company_name,
    }
}

function transformHubspotToAIAContactSchema(rawData){
    return {
        hubspot_id: rawData.id,
        agagt_: rawData.agagt_, //Custom
        company: rawData.company,
        contact2: rawData.firstname,
        contact3: rawData.lastname,
        email_addr: rawData.email,
        address: rawData.address,
        city: rawData.city,
        state: rawData.state,
        zip_code: rawData.zip,
        phone: rawData.phone,
        cell_phone: rawData.mobilephone,
        fax: rawData.fax, //Custom
        url: rawData.website,
        email_type: rawData.email_type, //Custom
        toBeRemoved:rawData.toBeRemoved,
        br_code:rawData.br_code,
        group_agency__company_: rawData.group_agency__company_,
        hs_parent_company_id: rawData.hs_parent_company_id,
        parent_company_name: rawData.parent_company_name,
    }
}

function transformToHubspotV1PropertyArray(data){
    const properties = [];
    Object.keys(data).forEach(prop => {
        properties.push({"name": prop, "value": data[prop]});
    })
    return properties;
}

function transformHubspotCompanyToSimpleObject(companies){
    return companies.map(company => {
        const {
            companyId,
            portalId,
            properties
        } = company;
        const simpleProperties = {}
        Object.keys(properties).forEach(property => {
            simpleProperties[property] = properties[property].value
        })

        return {
            companyId,
            portalId,
            ...simpleProperties
        }
    })
}

function transformHubspotContactToSimpleObject(contacts){
    return contacts.map( contact => {
        const {id,properties} = contact
        return {id, ...properties};
    })
}

function attachCompanyInformationToContactsForExport(companies=[],contacts=[]){
    return contacts.map( contact => {
        companies.forEach( company => {
            if(company.agagt_ === contact.agagt_){
                contact.company = company.name
                return
            }
        })
        return contact
    })
}

module.exports={
    companyProperties,
    contactProperties,
    transformAIAToHubspotCompanySchema,
    transformAIAToHubspotContactSchema,
    transformHubspotToAIACompanySchema,
    transformHubspotToAIAContactSchema,
    transformToHubspotV1PropertyArray,
    transformHubspotCompanyToSimpleObject,
    transformHubspotContactToSimpleObject,
    attachCompanyInformationToContactsForExport
}