const {
    sendMessage,
    ...settings
} = require('./settings')
const csv = require('fast-csv')
 
const log = require('./verboseLog')
 
const {
    chunk,
    progressUpdate
} = require('./utilities')
 
const {
    transformAIAToHubspotCompanySchema,
    transformAIAToHubspotContactSchema,
    transformToHubspotV1PropertyArray,
    transformHubspotContactToSimpleObject,
} = require('./shapeTransforms')
 
const {
    exportContactsFromHubspot,
    exportCompaniesFromHubspot
} = require('./exportFromHubspot')
 
async function uploadCompaniesToHubspot(fileLocation, currentCompanies, options) {
    let fileRows = [];
    csv.parseFile(fileLocation, {
            headers: true
        })
        .on('error', error => {
            // Log error to console
            log(error, 0)
        })
        .on('data', (data) => {
            // Add data to rows
            fileRows.push(data)
        })
        .on('end', async () => {
            log(`File read successfully.`)
 
 
            // convert rows to a format hubspot will accept
            let importedCompanies = fileRows.map(transformAIAToHubspotCompanySchema) // match hubspot properties
 
            let {
                toAdd,
                toUpdate,
                toDelete
            } = compareCompanies(currentCompanies, importedCompanies)
 
            toUpdate = toUpdate.map( company => {
                delete company.portalId;
                return company
            })
            const deleteRequests = deleteCompaniesRequests(toDelete)
            const createRequests = createCompaniesRequests(toAdd)
            const updateRequests = updateCompaniesRequests(toUpdate)
 
            log(`Deleting ${deleteRequests.length} Companies`)
            log(`Adding ${createRequests.length} Companies`)
            log(`Updating ${updateRequests.length} Companies`)
 
            const requests = createRequests.concat(deleteRequests,updateRequests)
 
            const activeRequests = requests.map((req, index) => {
                const {
                    url,
                    ...options
                } = req;
                return sendMessage(url, options)
            })
 
            return await progressUpdate(activeRequests, message => log(`${message} - Companies Uploaded`, 1))
 
 
        })
 
}
 
async function uploadContactsToHubspot(fileLocation, currentCompanies, options) {
    let fileRows = [];
 
    const currentContacts = await exportContactsFromHubspot()
 
    csv.parseFile(fileLocation, {
            headers: true
        })
        .on('error', error => {
            // Log error to console
            log(error, 0)
            return false
        })
        .on('data', (data) => {
            // Add data to rows
            fileRows.push(data)
        })
        .on('end', async () => {
            log(`Contact File read successfully.`)
 
 
        // convert rows to a format hubspot will accept
        let importedContacts = fileRows
            .map(transformAIAToHubspotContactSchema) // match hubspot properties
 
        const {toAdd,toUpdate,toDelete} = compareContacts(currentContacts,importedContacts)
 
        log(`Adding ${toAdd.length} Contacts`)
 
        const createRequests = createContactsRequests(toAdd)
        const updateRequests = updateContactsRequests(toUpdate)
        const deleteRequests = deleteContactsRequests(toDelete)
 
        const requests = createRequests.concat(updateRequests,deleteRequests)
 
        const activeRequests = requests.map((req, index) => {
            const {
                url,
                ...options
            } = req;
            return sendMessage(url, options)
        })
 
        await progressUpdate(activeRequests, message => log(`${message} - Uploading Contacts`, 1))
 
        const hubspotExportedContacts = await exportContactsFromHubspot()
        const associations = await associateContactsToCompany(hubspotExportedContacts, currentCompanies)
    })
}
 
async function associateContactsToCompany(contacts, companies) {
    const associations = contacts.map(contact => {
        const matchingCompany = companies.find(company => company.agagt_ === contact.properties.agagt_);
        if (matchingCompany) {
            return {
                from: {
                    id: contact.id
                },
                to: {
                    id: matchingCompany && matchingCompany.companyId
                },
                type: 'contact_to_company'
            }
        } else {
            return
        }
    }).filter(item => item !== undefined)
 
    const chunkedAssociations = chunk(associations, 100)
 
    const requests = chunkedAssociations.map(associations => {
        return {
            url: `${settings.baseHubspotUrl}/crm/v3/associations/contact/company/batch/create`,
            method: 'post',
            headers: {
                'Authorization': `Bearer ${settings.hapikey}`,
                'Content-Type': 'application/json'
              },
            // searchParams: {
            //     hapikey: settings.hapikey
            // },
            json: {
                inputs: associations
            }
        }
    })
 
    const activeRequests = requests.map(req => {
        const {
            url,
            ...options
        } = req
        return sendMessage(url, options).catch(err => log(err, 0))
    })
 
    return progressUpdate(activeRequests, message => log(`${message} - Associating Contacts to Companies`, 1))
}
 
const fs = require('fs');
const path = require('path');

function compareCompanies(currentCompanies, importCompanies) {
    const toAdd = [];
    const toUpdate = [];
    const toDelete = [];
    const processedNames = new Set();
    const skippedEntries = [];

    importCompanies.forEach(imp => {
        // Check for duplicate company names
        if (processedNames.has(imp.name)) {
            skippedEntries.push({ name: imp.name, companyId: imp.companyId }); // Log skipped entry
            return; // Skip processing if the name is already handled
        }
        processedNames.add(imp.name);

        // Find the company through unique identifiers or name
        const existingCompany = currentCompanies.find(cur =>
            cur.companyId.toString() === imp.companyId ||
            cur.agagt_ === imp.agagt_ ||
            cur.name === imp.name
        );

        if (existingCompany) {
            if (imp.toBeRemoved === 'yes') {
                toDelete.push(imp);
            } else {
                // Clean up empty or undefined fields in `imp`
                for (const key in imp) {
                    if (imp.hasOwnProperty(key) && (imp[key] === '' || imp[key] === undefined)) {
                        delete imp[key];
                    }
                }
                toUpdate.push(Object.assign({}, existingCompany, imp));
            }
        } else {
            delete imp.toBeRemoved;
            toAdd.push(imp);
        }
    });

    // Create the directory if it doesn't exist
    const directoryPath = path.join(__dirname, './new-test-file');
    if (!fs.existsSync(directoryPath)) {
        fs.mkdirSync(directoryPath, { recursive: true });
    }

    // Create CSV content for skipped entries
    const csvHeader = 'Name,CompanyId\n';
    const csvRows = skippedEntries.map(entry => `${entry.name},${entry.companyId}`).join('\n');
    const csvContent = csvHeader + csvRows;

    // Write CSV file to disk
    const filePath = path.join(directoryPath, 'skipped_entries.csv');
    fs.writeFileSync(filePath, csvContent, 'utf8');

    console.log(`Skipped entries saved to ${filePath}`);
    console.log(`Count of skipped entries: ${skippedEntries.length}`);

    return {
        toAdd,
        toUpdate,
        toDelete
    };
}



 
function compareContacts(currentContacts,importContacts){
    const toAdd = []
    const toUpdate = []
    const toDelete = []
    currentContacts.forEach( currr => {
        // console.log('cur',currr)
    })
    importContacts.forEach( imp => {
        // console.log('imoer',imp)
        // We're using Hubspot IDs to determine if the object already exists.
        const existingContact = currentContacts.find( cur => (cur.id == imp.id))
        // delete imp.id // id can't be part of the uploaded properties so we remove it from the object.
        // console.log('out',existingContact)
        if(existingContact){
            // const updatedContact = {
            //     id: existingContact.id, //affirm the id from the existing contact
            //     properties: imp // pass the properties into the updated object.
            // }
            if(imp.toBeRemoved == 'yes'){
                toDelete.push(imp)
            }else{
                delete imp.toBeRemoved;
                delete imp.id;
                    for (const key in imp) {
                        if (imp.hasOwnProperty(key)) {
                            if(imp[key] == '' || imp[key] == undefined){
                                delete imp[key]
                            }
                        }
                    }
                const updatedContact = {
                    id: existingContact.id, //affirm the id from the existing contact
                    properties: imp // pass the properties into the updated object.
                }
                toUpdate.push(updatedContact) // add the contact to the update list
            }
               
        } else {
            // console.log('add',imp)
            delete imp.toBeRemoved;
            delete imp.lastmodifieddate;
            delete imp.hs_object_id;
            delete imp.createdate;
            delete imp.id;
            toAdd.push(imp) // no found match, add the contact to the new list.
        }
    })
 
    return ({toAdd,toUpdate,toDelete})
}
 
function createCompaniesRequests(companiesToCreate) {
    const companies = companiesToCreate
        .map(company => {
            const {
                companyId: id,
                ...properties
            } = company
            return {
                id,
                properties
            }
        })
 
    return chunk(companies, 100).map(row => {
        return {
            url: `${settings.baseHubspotUrl}/crm/v3/objects/companies/batch/create`,
            // url: `${settings.baseHubspotUrl}/crm/v3/objects/companies/batch/create?hapikey=${settings.hapikey}`,
            method: 'post',
            json: {
                inputs: row
            },
            headers: {
                'Authorization': `Bearer ${settings.hapikey}`,
                'Content-Type': 'application/json'
              },
            responseType: 'json'
        }
    })
}
 
function deleteCompaniesRequests(companiesToDelete) {
    console.log('get data',companiesToDelete)
    const companies = companiesToDelete
        .map(company => {
            const {
                companyId: id
            } = company
            return {
                id
            }
        })
    return chunk(companies, 100).map(row => {
        console.log('row',row)
        return {
            url: `${settings.baseHubspotUrl}/crm/v3/objects/companies/batch/archive`,
            // url: `${settings.baseHubspotUrl}/crm/v3/objects/companies/batch/archive?hapikey=${settings.hapikey}`,
            method: 'post',
            json: {
                inputs: row
            },
            headers: {
                'Authorization': `Bearer ${settings.hapikey}`,
                'Content-Type': 'application/json'
              },
            responseType: 'json'
        }
    })
}
 
function updateCompaniesRequests(companiesToUpdate) {
    const companies = companiesToUpdate
        .map(company => {
            // if(company == ''){
            //     delete company;
            // }
            const {
                companyId: id,
                ...properties
            } = company
            return {
                id,
                properties
            }
        })
    return chunk(companies, 100)
        .map(row => {
            return {
                url: `${settings.baseHubspotUrl}/crm/v3/objects/companies/batch/update`,
                // url: `${settings.baseHubspotUrl}/crm/v3/objects/companies/batch/update?hapikey=${settings.hapikey}`,
                method: 'post',
                json: {
                    inputs: row,
                },
                headers: {
                    'Authorization': `Bearer ${settings.hapikey}`,
                    'Content-Type': 'application/json'
                  },
                responseType: 'json'
            }
        })
}
 
function createContactsRequests(contactsToCreate) {
 
    contactsToCreate = contactsToCreate.map(contact => {
        return {
            properties: contact
        }
    })
 
    // Chunk requests for batching
    const chunkedContacts = chunk(contactsToCreate, 10)
 
    return chunkedContacts.map(row => {
        return {
            url: `${settings.baseHubspotUrl}/crm/v3/objects/contacts/batch/create`,
            // url: `${settings.baseHubspotUrl}/crm/v3/objects/contacts/batch/create?hapikey=${settings.hapikey}`,
            method: 'post',
            json: {
                inputs: row
            },
            headers: {
                'Authorization': `Bearer ${settings.hapikey}`,
                'Content-Type': 'application/json'
              },
            responseType: 'json'
        }
    })
}
function updateContactsRequests(contactsToUpdate) {
 
    contactsToUpdate = contactsToUpdate.map(contact => {
        const {id, properties} = contact;
 
        return {
            id,
            properties
        }
    })
 
    // Chunk requests for batching
    const chunkedContacts = chunk(contactsToUpdate, 10)
 
    return chunkedContacts.map(row => {
        console.log('uptodate',row)
        return {
            url: `${settings.baseHubspotUrl}/crm/v3/objects/contacts/batch/update`,
            // url: `${settings.baseHubspotUrl}/crm/v3/objects/contacts/batch/update?hapikey=${settings.hapikey}`,
            method: 'post',
            json: {
                inputs: row
            },
            headers: {
                'Authorization': `Bearer ${settings.hapikey}`,
                'Content-Type': 'application/json'
              },
            responseType: 'json'
        }
    })
}
 
function deleteContactsRequests(contactsToDelete) {
    console.log('get data',companiesToDelete)
    contactsToDelete = contactsToDelete.map(contact => {
        const {id, properties} = contact;
 
        return {
            id,
            properties
        }
    })
 
    // Chunk requests for batching
    // const chunkedContacts = chunk(contactsToDelete, 10)
    return chunk(contactsToDelete, 100).map(row => {
        console.log('row',row)
        return {
            url: `${settings.baseHubspotUrl}/crm/v3/objects/contacts/batch/archive`,
            // url: `${settings.baseHubspotUrl}/crm/v3/objects/contacts/batch/archive?hapikey=${settings.hapikey}`,
            method: 'post',
            json: {
                inputs: row
            },
            headers: {
                'Authorization': `Bearer ${settings.hapikey}`,
                'Content-Type': 'application/json'
              },
            responseType: 'json'
        }
    })
}
 
module.exports = {
    uploadCompaniesToHubspot,
    uploadContactsToHubspot,
};