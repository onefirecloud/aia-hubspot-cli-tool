// ConfigStore for easy config options
const Configstore = require('configstore')
const packageJson = require('./package.json')

const config = new Configstore(packageJson.name)

module.exports = config