function chunk(array, chunkSize) {
    const chunkedArray = []
    for (let i = 0, j = array.length; i < j; i += chunkSize) {
        chunkedArray.push(array.slice(i, i + chunkSize))
    }
    return chunkedArray
}

function progressUpdate(promises, callback) {
    let d = 0;
    callback('0%')
    promises.forEach(p => p.then(() => {
        d++;
        callback(`${Math.floor((d * 100) / promises.length)}%`)
    }))
    return Promise.allSettled(promises)
}

module.exports = {
    chunk,
    progressUpdate
}