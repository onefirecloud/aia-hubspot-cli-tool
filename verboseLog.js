var argv = require('minimist')(process.argv.slice(2))

// priority 0 is forced even on --quiet
// priority 1 is regular
// priority 2 is verbose only
function log(message, priority = 2){
    if((argv.q || argv.quiet) && priority !== 0){
        //Shhh
        return
    }
    if(priority <= 1 || (argv.v || argv.verbose)){
        console.log(message)
        return
    }
}

module.exports=log