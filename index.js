#!/usr/bin/env node



// Get our process arguments
var argv = require('minimist')(process.argv.slice(2))
var http = require('http');
const {
    transformHubspotCompanyToSimpleObject,
    transformHubspotContactToSimpleObject,
    transformHubspotToAIAContactSchema,
    transformHubspotToAIACompanySchema,
    attachCompanyInformationToContactsForExport,
} = require('./shapeTransforms')

const config = require('./configStore')

const {importCustomPropertiesFromFile} = require('./properties')

// Custom logger for verbose only logs.
const log = require('./verboseLog.js')

// Condense argument options
const exportPath = argv.e || argv.export
const companiesPath = argv.companies
const contactsPath = argv.contacts

const key = argv.key
const importProperties = argv.importproperties
// console.log(argv);

const {uploadCompaniesToHubspot,uploadContactsToHubspot} = require('./uploadToHubspot')

const {
    exportCompaniesFromHubspot,
    exportContactsFromHubspot,
    writeCSVtoFile
} = require('./exportFromHubspot.js')

function logHelp(){
console.log(
`
--key           : Run blank to reveal current API Key, follow with key to set API Key Eg. 'node index.js --key AND node index.js --key 'e611463a-7e95-47d5-945d-8ecfb08e5d99''
--help -h -?    : Display this help text
--companies     : Path to the companies csv to import
--contacts      : Path to the contacts csv to import
--export -e     : Path to the directory to export the hubspot companies and contacts CSV files "node index.js --e tmp/csv"
-q              : Quiet, suppress console logs to just errors
-v              : Verbose, show all console logs for debugging
`
)
}

(async function () {
    // http.createServer(function (req, res) {
    //     res.writeHead(200, {'Content-Type': 'text/html'});
    //     res.end('Hello Worldsass!');
    //   }).listen(8080);
    if(argv.help || argv.h || argv['?'] || Object.keys(argv).length === 1){
        logHelp()
    }
    // console.log(argv.key)
    //If key argument is passed set or retrieve key
    if(typeof key === 'string'){
        config.set('hapikey',key)
    }
    if(key === true){
        log(config.get('hapikey'),0)
    }

    //Update properties
    if(importProperties){
        importCustomPropertiesFromFile(importProperties)
    }

    if(companiesPath || contactsPath){
        // Get Companies for later use
        let currentCompanies = await exportCompaniesFromHubspot().then(transformHubspotCompanyToSimpleObject)

        // Process if company import option is passed
        if (companiesPath) {
            
            uploadCompaniesToHubspot(companiesPath, currentCompanies)
        }
        // Process if contact import option is passed.
        if (contactsPath){
            currentCompanies = await exportCompaniesFromHubspot().then(transformHubspotCompanyToSimpleObject)
            uploadContactsToHubspot(contactsPath, currentCompanies)
        }
    }



    // Process if export option present
    // log(exportPath,1)
    if (exportPath) {
        log('Getting information from Hubspot...',1)
        const currentCompanies = await exportCompaniesFromHubspot()
        const currentContacts = await exportContactsFromHubspot()

        const simpleCompanies = transformHubspotCompanyToSimpleObject(currentCompanies)
        const simpleContacts = transformHubspotContactToSimpleObject(currentContacts)

        const simpleContactsWithCompany = attachCompanyInformationToContactsForExport(simpleCompanies,simpleContacts)

        const transformedCompanies = simpleCompanies.map(transformHubspotToAIACompanySchema)
        const transformedContacts = simpleContactsWithCompany.map(transformHubspotToAIAContactSchema)


        log(`Writing ${transformedCompanies.length} companies to ${exportPath}/companies.csv`,1)
        writeCSVtoFile(exportPath+"/companies.csv", transformedCompanies)
        log(`Writing ${transformedContacts.length} contacts to ${exportPath}/contacts.csv`,1)
        writeCSVtoFile(exportPath+"/contacts.csv", transformedContacts)

    }

})()

