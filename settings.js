// Load our .env config
require('dotenv').config()
const got = require('got')
const log = require('./verboseLog')

const config = require('./configStore')

// Queue for rate-limited API calls
const Queue = require('smart-request-balancer');

// Set out custom handler for requests
const queue = new Queue({
    rules:{
        hubspot:{
            rate: 100,
            limit: 10,
            priority: 1,
        },
    },
    overall:{
        rate: 100,
        limit: 10
    },
    retryTime: 16,
})

const sendMessage = (url, options) => queue.request(retry => got(url, options)
    .then(response => {
        return response
    })
    .catch(error => {

        if (error.response.statusCode === 409) {
            //This means there was already an existing record we really don't need to worry about this.
            //Hubspot updates the data anyways which is what we want.
            //log(error.response.body,2)
            return
        }
        if ([429, 500, 502].includes(error.response.statusCode)) {
            return retry()
        }

        if (error) {
            log(error.response.body, 0)
        }
        throw error;
    }), "hubspot", "hubspot")

module.exports = {
    baseHubspotUrl: 'https://api.hubapi.com',
    // baseHubspotUrl: 'https://api.hubapi.com',
    hapikey: config.get('hapikey'),
    queue,
    sendMessage
}