const settings = require('./settings')
const csv = require('fast-csv')

const fs = require('fs')
const log = require('./verboseLog')
const got = require('got')

const {companyProperties,contactProperties} = require('./shapeTransforms');

async function exportCompaniesFromHubspot() {
    const options = {
        method: 'get',
        responseType: 'json',
        headers: {
            'Authorization': `Bearer ${settings.hapikey}`,
            'Content-Type': 'application/json'
          }
    }

    // Hubspot uses these weird duplicated query parameters so we can't use the 
    // default options that gotjs gives us so we just build the query string manually
    
    const properties = companyProperties
        .map(item => 'properties='+item)
        .join('&')

    const url = `${settings.baseHubspotUrl}/companies/v2/companies/paged?limit=100&${properties}`
    // const url = `${settings.baseHubspotUrl}/companies/v2/companies/paged?limit=100&hapikey=${settings.hapikey}&${properties}`

    const companies = await getAllCompaniesFromHubspot(url, options)
    return companies;
}

async function exportContactsFromHubspot(){
    // var myheader = ('Authorization', 'Bearer ' + settings.hapikey);
    const options = {
        method: 'get',
        responseType: 'json',
        headers: {
            'Authorization': `Bearer ${settings.hapikey}`,
            'Content-Type': 'application/json'
          }
    }

    // Hubspot uses these weird duplicated query parameters so we can't use the 
    // default options that gotjs gives us so we just build the query string manually

    const properties = contactProperties
        .map( item => 'properties='+item)
        .join("&")

    const url = `${settings.baseHubspotUrl}/crm/v3/objects/contacts?limit=100&${properties}`
    // const url = `${settings.baseHubspotUrl}/crm/v3/objects/contacts?limit=100&hapikey=${settings.hapikey}&${properties}`

    const contacts = await getAllContactsFromHubspot(url,options)
    return contacts
}

const getAllCompaniesFromHubspot = async (url, options, offset, companies=[]) => {
        const response = await sendRequest(offset ? url+'&offset='+offset : url, options)
        const {body} = response
        body.companies.forEach( company => companies.push(company))
        log(`Downloading Companies from Hubspot - ${companies.length}`,2)

        if(body['has-more']){
            return( getAllCompaniesFromHubspot(url, options, body.offset, companies) )
        } else {
            return companies
        }
}

const getAllContactsFromHubspot = async (url,options,offset,contacts=[]) => {
    const response = await sendRequest(offset ? url+'&after='+offset : url, options)
    const {body} = response
    body.results.forEach( contact => contacts.push(contact))
    log(`Downloading Contacts from Hubspot - ${contacts.length}`,2)

    if(body.paging && body.paging.next){
        return (getAllContactsFromHubspot(url, options, body.paging.next.after, contacts))
    } else {
        return contacts
    }
}

const sendRequest = (url,options) =>
    settings.queue.request( retry => got(url,options)
        .catch(error => log(error,0))
        , 'hubspot', 'hubspot');


function writeCSVtoFile(filePath = ".", data){
    const csvStream = csv.format({headers:true});
    const writeStream = fs.createWriteStream(filePath)

    csvStream.pipe(writeStream).on('end', () => process.exit());

    data.forEach( entry => {
        csvStream.write(entry)
    })

    csvStream.end();
}


module.exports = {
    exportCompaniesFromHubspot,
    exportContactsFromHubspot,
    writeCSVtoFile
}
